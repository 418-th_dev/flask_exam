FROM python:3.7-slim

ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY ./requirements.txt /code/
RUN apt update
RUN apt install -y gcc libpq-dev python3-dev
RUN pip3 install -r /code/requirements.txt
COPY . /code

WORKDIR /code

ENV FLASK_APP 'app'
ENV FLASK_RUN_HOST '127.0.0.1'
ENV FLASK_RUN_PORT '5000'

EXPOSE 5000

CMD python manage.py db migrate; python manage.py db upgrade; python manage.py runserver --host ${FLASK_RUN_HOST} --port ${FLASK_RUN_PORT}
