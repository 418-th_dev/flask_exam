#!/bin/sh

pip install -r requirements.txt

python manage.py db init

if [ "$(python manage.py db migrate)" != "No changes detected" ]; then
  python manage.py db upgrade
fi

exec $@