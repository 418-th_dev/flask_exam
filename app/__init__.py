from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_ckeditor import CKEditor
from flask_login import LoginManager
from flask_cors import CORS
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec import APISpec
from flask_apispec.extension import FlaskApiSpec
from app.settings import Config

cors = CORS()
bootstrap = Bootstrap()
db = SQLAlchemy()
ckeditor = CKEditor()


def create_app(config_object=Config):
    app = Flask(__name__)
    app.config.from_object(config_object)

    db.init_app(app)

    bootstrap.init_app(app)
    if app.config['CORS_ON']:
        cors.init_app(app)
    ckeditor.init_app(app)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    docs = FlaskApiSpec()

    docs.init_app(app)

    app.config.update({
        'APISPEC_SPEC': APISpec(
            title='some_title',
            version='v1',
            openapi_version='2.0',
            plugins=[MarshmallowPlugin()],
        ),
        'APISPEC_SWAGGER_URL': '/swagger/'
    })

    # blueprint for non-auth parts of app
    from .views import homepage as homepage_blueprint
    app.register_blueprint(homepage_blueprint)

    # blueprint for non-auth parts of app
    from .views import profile as profile_blueprint
    app.register_blueprint(profile_blueprint)

    # blueprint for auth routes in our app
    from .views import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    return app
