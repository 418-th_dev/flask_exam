from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from .. import db
from ..models import User

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():

    if request.method == 'GET':
        return render_template('login.html')

    elif request.method == 'POST':

        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False
        user = User.query.filter_by(email=email).first()

        if not user or not check_password_hash(user.password, password):
            flash('Wrong password or email.')
            return redirect(url_for('auth.login'))

        login_user(user, remember=remember)
        return redirect(url_for('profile.index'))


@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        return render_template('signup.html')
    elif request.method == 'POST':
        data = {
            'email': request.form.get('email'),
            'name': request.form.get('name'),
            'password': request.form.get('password'),
            'is_super_user': request.form.get('is_super_user'),
        }

        is_not_valid = validator(email=request.form.get('email'), name=request.form.get('name'))

        if is_not_valid:
            flash(is_not_valid)
            return redirect(url_for('auth.signup'))

        data['password'] = generate_password_hash(request.form.get('password'), method='sha256')
        user = User(**data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('auth.login'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('homepage.index'))


def validator(name, email):
    if User.query.filter_by(email=email).first():
        return 'User with same email already created'
    elif User.query.filter_by(name=name).first():
        return 'User with same username already created'
    else:
        return False
