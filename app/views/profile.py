from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from .. import db
from ..models import User
from .auth import validator

profile = Blueprint('profile', __name__)


@profile.route('/profile', methods=['GET'])
@login_required
def index():
    if request.method == 'GET':
        users_to_edit = []
        if current_user.is_super_user:
            users_to_edit = User.query.filter_by(is_super_user=False).all()
        else:
            users_to_edit.append(current_user)

        return render_template('profile.html', name=current_user.name, users_to_edit=users_to_edit)


@profile.route('/profile/<name>', methods=['POST'])
@login_required
def detail_profile(name):
    if current_user.is_super_user or current_user.name == name and request.method == 'POST':
        data = {
            'email': request.form.get('email'),
            'name': request.form.get('name'),
            'password': request.form.get('password'),
            'is_super_user': request.form.get('is_super_user')
        }

        is_not_valid = validator(name=request.form.get('name'),
                                 email=request.form.get('email'))
        if is_not_valid:
            flash(is_not_valid)
            return redirect(url_for('profile.index'))

        User.query.filter_by(name=name).update(**data)
        db.session.commit()
        flash('User edited successfully')
        return redirect(url_for('profile.index'))


@profile.route('/delete_user/<name>', methods=['DELETE'])
@login_required
def delete_user(name):
    user = User.query.filter_by(name=name).first()
    db.session.delete(user)
    return redirect(url_for('profile.index'))