import os
from dotenv import load_dotenv
from envparse import Env

load_dotenv()
env = Env()
basedir = os.path.abspath(os.path.dirname(__file__))
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


class Config:
    # BASE SETTINGS
    DEBUG = True if os.environ.get('DEBUG') else False
    CORS_ON = True if os.environ.get('CORS_ON') else False
    SECRET_KEY = os.environ.get('SECRET_KEY')
    FLASK_APP = os.environ.get('FLASK_APP')
    PUBLIC_URL = os.getenv('PUBLIC_URL')
    POSTGRES_DB = os.getenv('POSTGRES_DB')
    POSTGRES_USER = os.getenv('POSTGRES_USER')
    POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD')
    POSTGRES_HOST = os.getenv('POSTGRES_HOST')
    POSTGRES_PORT = os.getenv('POSTGRES_PORT')
    # SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')

    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}
    SQLALCHEMY_TRACK_MODIFICATIONS = True if os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS') else False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    TESTING = os.environ.get('TESTING')
    REDIS_HOST = os.environ.get('REDIS_HOST')
    REDIS_PORT = os.environ.get('REDIS_PORT')

    CKEDITOR_SERVE_LOCAL = False
    CKEDITOR_HEIGHT = 400
    BASE_DIR = basedir

    @staticmethod
    def init_app(app):
        pass


config = {
    'default': Config
}
