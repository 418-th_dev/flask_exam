import asyncio
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from app import create_app, db
from app.models import User
import uuid
from sqlalchemy import update

app = create_app()
migrate = Migrate(app, db)
db.create_all(app=app)


def make_shell_context():
    return dict(app=app, db=db, User=User, task_manager=task_manager())


manager = Manager(app)
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


async def task_manager():
    task1 = data_in_range(1, 11, 31, 41)

    task2 = data_in_range(11, 21, 41, 51)

    task3 = data_in_range(21, 31, 51, 60)

    await asyncio.wait([task1, task2, task3])


async def data_in_range(first_range_from,
                        first_range_to,
                        second_range_to,
                        second_range_from):
    for item in range(first_range_from, first_range_to):
        update(User).where(User.id == item).values(name=str(uuid.uuid4()))

    for item in range(second_range_to, second_range_from):
        update(User).where(User.id == item).values(name=str(uuid.uuid4()))


if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(task_manager())
    except Exception as exp:
        print(exp)
    manager.run()
