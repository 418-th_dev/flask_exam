Создаем и настраиваем виртуальное окружение

```bash
python3.8 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Создаем миграции

```bash
python3 manage.py db migrate
```

Применяем

```bash
python3 manage.py db upgrade
```

Поднимаем сервер

```bash
python3 manage.py runserver
```